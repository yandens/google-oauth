const router = require('express').Router()
const passport = require('passport')
const middleware = require('../middlewares/auth')

// index
router.get('/', (req, res, next) => {
  res.render('login')
})

router.get('/test', (req, res, next) => {
  res.render('test')
})

router.get('/auth/google', passport.authenticate('google', {
  scope: ['profile', 'email']
}))

router.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/success',
  failureRedirect: '/error'
}), (req, res) => { res.redirect('/success') })

router.get('/success', middleware.isLogin, (req, res) => {
  res.render('success', { user: req.user })
})

router.get('/error', (req, res) => res.send('error logging in'))

module.exports = router
